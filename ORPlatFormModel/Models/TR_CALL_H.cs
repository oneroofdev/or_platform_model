﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public class TR_CALL_H
    {
        [Key] 
        [StringLength(10)]
        [Column(TypeName = "varchar", Order = 0)]
        public string TR_CALL_H_ID { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string CALLER_ID { get; set; }
        public int? CORP_ID { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string CONTACT_NAME { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CONTACT_PHONE { get; set; } 
        public DateTime? RECEIVE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string RECEIVE_BY { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string FINISH_CALL { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string CALLER_TYPE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
