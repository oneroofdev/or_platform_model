﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public partial class MT_SEND_CONFIG
    {
        [Key]
        [Column(TypeName = "bigint")]
        public long SEND_ID { get; set; }
        public int DEFINE_TIME_OUT { get; set; }
        [StringLength(30)]
        public string OTP_REF_CODE { get; set; }
        [Column(TypeName = "bigint")]
        public long SEND_CHANNEL_ID { get; set; }
        [Column(TypeName = "bigint")]
        public long TEMPLATE_ID { get; set; }
        [StringLength(1)]
        public string ACTIVE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
