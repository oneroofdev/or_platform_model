﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public class TR_CALL_D
    {
        [Key]
        [StringLength(10)]
        [Column(TypeName = "varchar", Order = 0)]
        public string TR_CALL_D_ID { get; set; }
        [Key]
        [StringLength(10)]
        [Column(TypeName = "varchar", Order = 1)]
        public string TR_CALL_H_ID { get; set; }
        public int QUESTION_ID { get; set; }
        public int ANSWER_ID { get; set; }
        public int? BRANCH_ID { get; set; }
        [StringLength(20)]
        [Column(TypeName = "varchar")]
        public string CONTACT_CHANEL { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string REMARK_DET { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string SERIOUS_CASE { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string FOLLOW_CASE { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string FINISH_CASE { get; set; }
        public DateTime? FINISH_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string FINISH_BY { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
