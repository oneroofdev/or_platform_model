﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public class MT_CORPORATE_ADDRESS
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CORP_ADDR_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        public int CORP_ID { get; set; }
        [StringLength(2)]
        [Column(TypeName = "varchar")]
        public string CORP_ADDR_TYPE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string ADDR_NO { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string BUILDING_NAME { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string FLOOR { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string VILLAGE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string MOO { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string SOI { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string ROAD { get; set; }
        public int? SUBDISTRICT_CODE { get; set; }
        public int? DISTRICT_CODE { get; set; }
        public int? PROVINCE_CODE { get; set; }
        [StringLength(5)]
        [Column(TypeName = "varchar")]
        public string ZIPCODE { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string TEL { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string FAX { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
