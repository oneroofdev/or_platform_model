﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public class MT_EMPLOYEE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EMP_ID { get; set; }
        public int CORP_ID { get; set; }
        public int? CORP_EMP_ID { get; set; }
        public int? TITLE_ID { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string FNAME_TH { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string LNAME_TH { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string FNAME_EN { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string LNAME_EN { get; set; }
        public int DEPT_ID { get; set; }
        public int? POSITION_ID { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string IS_CONTACT { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string TEL { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string MOBILE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string FAX { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string EMAIL { get; set; }
        [StringLength(20)]
        [Column(TypeName = "varchar")]
        public string REFER_EMP_CODE { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string ACTIVE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
