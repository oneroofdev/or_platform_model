﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public class MT_DEPARTMENT
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DEPT_ID { get; set; }
        public int CORP_ID { get; set; }
        public int? CORP_DEPT_ID { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string DEPT_NAME_TH { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string DEPT_NAME_EN { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string IS_EXECUTIVE { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string VIEW_ALL_PROJECT { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
