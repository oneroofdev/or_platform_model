﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ORPlatFormModel.Models
{
    public class MT_CALL_QUESTION
    {
        [Key] 
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QUESTION_ID { get; set; }
        public int? CORP_ID { get; set; }
        [StringLength(2)]
        [Column(TypeName = "varchar")]
        public string LANGUAGE_TYPE { get; set; }
        [StringLength(150)]
        [Column(TypeName = "varchar")]
        public string QUESTION_DESC { get; set; } 
        public int QUESTION_TYPE_ID { get; set; }
        [StringLength(3)]
        [Column(TypeName = "varchar")]
        public string TYPE { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string IS_BRANCH { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string ACTIVE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }



    }
}
