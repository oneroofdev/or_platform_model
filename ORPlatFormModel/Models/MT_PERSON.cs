﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public class MT_PERSON
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] 
        public int PERSON_ID { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string PERSON_CODE { get; set; }
        public int CORP_ID { get; set; }
        public int? CORP_PERSON_ID { get; set; }
        [StringLength(3)]
        [Column(TypeName = "varchar")]
        public string PRIORITY { get; set; }
        [StringLength(3)]
        [Column(TypeName = "varchar")]
        public string MEMBER_TIER { get; set; }
        public int? TITLE_ID { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string COMPANY_NAME { get; set; }
        [StringLength(150)]
        [Column(TypeName = "varchar")]
        public string FNAME_TH { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string LNAME_TH { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string FNAME_EN { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string LNAME_EN { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string GENDER { get; set; }
        public DateTime? BIRTHDAY { get; set; }
        [StringLength(15)]
        [Column(TypeName = "varchar")]
        public string IDCARD { get; set; }
        [StringLength(15)]
        [Column(TypeName = "varchar")]
        public string PASSPORT { get; set; }
        [StringLength(30)]
        [Column(TypeName = "varchar")]
        public string PHONE { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string MOBILE { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string MOBILE_SMS { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string MOBILE_ALT { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string EMAIL { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string EMAIL_ALT { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string LINEID { get; set; }
        [StringLength(100)]
        [Column(TypeName = "varchar")]
        public string FACEBOOK_ID { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string CHANNEL { get; set; }
        [Column(TypeName = "text")]
        public string IMAGE_PROFILE_PATH { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string CUST_TYPE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
