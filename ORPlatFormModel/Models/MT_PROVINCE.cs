﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public class MT_PROVINCE
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TRAN_ID { get; set; }
        [Key]
        [Column(Order = 0)]
        public int PROVINCE_CODE { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string PROV_NAME_TH { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string PROV_NAME_EN { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
