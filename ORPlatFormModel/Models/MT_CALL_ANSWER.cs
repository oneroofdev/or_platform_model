﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ORPlatFormModel.Models
{
    public class MT_CALL_ANSWER
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ANSWER_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        public int QUESTION_ID { get; set; }
        public int? CORP_ID { get; set; }
        [StringLength(2)]
        [Column(TypeName = "varchar")]
        public string LANGUAGE_TYPE { get; set; }
        [StringLength(500)]
        [Column(TypeName = "varchar")]
        public string ANSWER_DESC { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string FOLLOW_CASE { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string ACTIVE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
