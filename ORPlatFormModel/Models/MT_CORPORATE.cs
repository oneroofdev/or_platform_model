﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORPlatFormModel.Models
{
    public class MT_CORPORATE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CORP_ID { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string CORP_NAME_TH { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string CORP_NAME_EN { get; set; }
        [StringLength(10)]
        [Column(TypeName = "varchar")]
        public string CORP_SHORT_NAME { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CORP_TEL { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CORP_FAX { get; set; }
        [StringLength(13)]
        [Column(TypeName = "varchar")]
        public string CORP_TAX_ID { get; set; }
        [Column(TypeName = "text")]
        public string LOGO_PATH { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CONTRACT_NO { get; set; }
        public int? CORP_TOTAL_ACCOUNT { get; set; }
        public int? TOTAL_PROJECT_AMOUNT { get; set; }
        public DateTime? CONTRACT_START_DATE { get; set; }
        public DateTime? CONTRACT_END_DATE { get; set; }
        public DateTime? DELIVER_LOGIN_DATE { get; set; }
        [StringLength(200)]
        [Column(TypeName = "varchar")]
        public string REMARK { get; set; }
        [StringLength(1)]
        [Column(TypeName = "varchar")]
        public string ACTIVE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
