﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ORPlatFormModel.Models
{
    public partial class TR_SEND_OTP
    {
        [Key]
        [Column(TypeName = "bigint")]
        public long LOG_ID { get; set; }
        [StringLength(100)]
        public string USER_LOGIN_NAME { get; set; }
        [StringLength(4)]
        public string REF_CODE { get; set; }
        [StringLength(6)]
        public string PASS_OTP { get; set; }
        [StringLength(6)]
        public string PASS_INPUT { get; set; }
        [StringLength(10)]
        public string SEND_TO { get; set; }
        [StringLength(1)]
        public string STATUS_OTP { get; set; }
        public DateTime? OTP_SEND_TIME { get; set; }
        public DateTime? OTP_TIME_OUT { get; set; }
        [Column(TypeName = "bigint")]
        public long TEMPLATE_ID { get; set; }
        [Column(TypeName = "bigint")]
        public long SEND_CHANNEL_ID { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(50)]
        [Column(TypeName = "varchar")]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
