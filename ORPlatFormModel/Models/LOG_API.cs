namespace ORPlatFormModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class LOG_API
    {
        public int Id { get; set; }

        public string Request { get; set; }

        public string Response { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public DateTime? RequestDateTime { get; set; }

        public DateTime? ResponseDateTime { get; set; }

        public string ModuleType { get; set; }

        public string ActionType { get; set; }

        [StringLength(100)]
        public string UserAccount { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        [StringLength(30)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(30)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [StringLength(30)]
        public string DeleteBy { get; set; }

        public DateTime? DeleteDate { get; set; }

        [StringLength(50)]
        public string TransactionId { get; set; }

        [StringLength(50)]
        public string test_api { get; set; }

        [StringLength(50)]
        public string test_api2 { get; set; }
    }
}
