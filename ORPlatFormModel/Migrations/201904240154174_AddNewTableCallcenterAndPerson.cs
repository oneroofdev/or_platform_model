namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewTableCallcenterAndPerson : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_CALL_ANSWER",
                c => new
                    {
                        ANSWER_ID = c.Int(nullable: false, identity: true),
                        QUESTION_ID = c.Int(nullable: false),
                        LANGUAGE_TYPE = c.String(),
                        ANSWER_DESC = c.String(maxLength: 150, unicode: false),
                        FOLLOW_CASE = c.String(maxLength: 1, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ANSWER_ID, t.QUESTION_ID });
            
            CreateTable(
                "dbo.MT_CALL_CHANNEL",
                c => new
                    {
                        CHANNEL_ID = c.Int(nullable: false, identity: true),
                        SEQ = c.Int(nullable: false),
                        LANGUAGE_TYPE = c.String(),
                        CHANNEL_NAME = c.String(maxLength: 50, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.CHANNEL_ID);
            
            CreateTable(
                "dbo.MT_CALL_QUESTION",
                c => new
                    {
                        QUESTION_ID = c.Int(nullable: false, identity: true),
                        LANGUAGE_TYPE = c.String(maxLength: 2, unicode: false),
                        QUESTION_DESC = c.String(maxLength: 100, unicode: false),
                        QUESTION_TYPE_ID = c.Int(nullable: false),
                        TYPE = c.String(maxLength: 3, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.QUESTION_ID);
            
            CreateTable(
                "dbo.MT_CALL_QUESTION_TYPE",
                c => new
                    {
                        QUESTION_TYPE_ID = c.Int(nullable: false, identity: true),
                        LANGUAGE_TYPE = c.String(maxLength: 2, unicode: false),
                        QUESTION_TYPE_DESC = c.String(maxLength: 100, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.QUESTION_TYPE_ID);
            
            CreateTable(
                "dbo.MT_PERSON",
                c => new
                    {
                        PERSON_ID = c.Int(nullable: false, identity: true),
                        CORP_ID = c.Int(nullable: false),
                        CORP_PERSON_ID = c.Int(),
                        TITLE_ID = c.Int(),
                        FNAME_TH = c.String(maxLength: 150, unicode: false),
                        LNAME_TH = c.String(maxLength: 200, unicode: false),
                        FNAME_EN = c.String(maxLength: 200, unicode: false),
                        LNAME_EN = c.String(maxLength: 200, unicode: false),
                        GENDER = c.String(maxLength: 1, unicode: false),
                        BIRTHDAY = c.DateTime(),
                        IDCARD = c.String(maxLength: 15, unicode: false),
                        PASSPORT = c.String(maxLength: 15, unicode: false),
                        MOBILE = c.String(maxLength: 10, unicode: false),
                        MOBILE_ALT = c.String(maxLength: 50, unicode: false),
                        EMAIL = c.String(maxLength: 100, unicode: false),
                        EMAIL_ALT = c.String(maxLength: 100, unicode: false),
                        LINEID = c.String(maxLength: 50, unicode: false),
                        FACEBOOK_ID = c.String(maxLength: 100, unicode: false),
                        CHANNEL = c.String(maxLength: 10, unicode: false),
                        IMAGE_PROFILE_PATH = c.String(unicode: false, storeType: "text"),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.PERSON_ID);
            
            CreateTable(
                "dbo.MT_PERSON_ADDRESS",
                c => new
                    {
                        TRAN_ID = c.Int(nullable: false, identity: true),
                        PERSON_ID = c.Int(nullable: false),
                        ADDR_TYPE = c.String(maxLength: 1, unicode: false),
                        BUILDING_NAME = c.String(maxLength: 50, unicode: false),
                        ADDR_NO = c.String(maxLength: 100, unicode: false),
                        FLOOR = c.String(maxLength: 50, unicode: false),
                        VILLAGE = c.String(maxLength: 100, unicode: false),
                        MOO = c.String(maxLength: 50, unicode: false),
                        SOI = c.String(maxLength: 50, unicode: false),
                        ROAD = c.String(maxLength: 50, unicode: false),
                        SUBDISTRICT_CODE = c.Int(),
                        DISTRICT_CODE = c.Int(),
                        PROVINCE_CODE = c.Int(),
                        ZIPCODE = c.String(maxLength: 5, unicode: false),
                        COMPANY_NAME = c.String(maxLength: 200, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TRAN_ID);
            
            CreateTable(
                "dbo.TR_CALL_D",
                c => new
                    {
                        TR_CALL_D_ID = c.String(nullable: false, maxLength: 10, unicode: false),
                        TR_CALL_H_ID = c.String(nullable: false, maxLength: 10, unicode: false),
                        QUESTION_ID = c.Int(nullable: false),
                        ANSWER_ID = c.Int(nullable: false),
                        CONTACT_CHANEL = c.String(maxLength: 20, unicode: false),
                        REMARK_DET = c.String(maxLength: 200, unicode: false),
                        SERIOUS_CASE = c.String(maxLength: 1, unicode: false),
                        FOLLOW_CASE = c.String(maxLength: 1, unicode: false),
                        FINISH_CASE = c.String(maxLength: 1, unicode: false),
                        FINISH_DATE = c.DateTime(),
                        FINISH_BY = c.String(maxLength: 30, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.TR_CALL_D_ID, t.TR_CALL_H_ID });
            
            CreateTable(
                "dbo.TR_CALL_H",
                c => new
                    {
                        TR_CALL_H_ID = c.String(nullable: false, maxLength: 10, unicode: false),
                        CALLER_ID = c.String(maxLength: 10, unicode: false),
                        CONTACT_NAME = c.String(maxLength: 100, unicode: false),
                        CONTACT_PHONE = c.String(maxLength: 50, unicode: false),
                        RECEIVE_DATE = c.DateTime(),
                        RECEIVE_BY = c.String(maxLength: 30, unicode: false),
                        FINISH_CALL = c.String(maxLength: 1, unicode: false),
                        CALLER_TYPE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TR_CALL_H_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_CALL_H");
            DropTable("dbo.TR_CALL_D");
            DropTable("dbo.MT_PERSON_ADDRESS");
            DropTable("dbo.MT_PERSON");
            DropTable("dbo.MT_CALL_QUESTION_TYPE");
            DropTable("dbo.MT_CALL_QUESTION");
            DropTable("dbo.MT_CALL_CHANNEL");
            DropTable("dbo.MT_CALL_ANSWER");
        }
    }
}
