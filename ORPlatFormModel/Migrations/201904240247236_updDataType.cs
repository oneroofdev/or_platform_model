namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updDataType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MT_CALL_ANSWER", "LANGUAGE_TYPE", c => c.String(maxLength: 2, unicode: false));
            AlterColumn("dbo.MT_CALL_CHANNEL", "LANGUAGE_TYPE", c => c.String(maxLength: 2, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MT_CALL_CHANNEL", "LANGUAGE_TYPE", c => c.String());
            AlterColumn("dbo.MT_CALL_ANSWER", "LANGUAGE_TYPE", c => c.String());
        }
    }
}
