namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdFiledLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MT_CALL_ANSWER", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_ANSWER", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_ANSWER", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_CHANNEL", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_CHANNEL", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_CHANNEL", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION_TYPE", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION_TYPE", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION_TYPE", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CAR_BRAND", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CAR_BRAND", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_CAR_BRAND", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_DEPARTMENT", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_DEPARTMENT", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_DEPARTMENT", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_DISTRICT", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_DISTRICT", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_DISTRICT", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON_ADDRESS", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON_ADDRESS", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON_ADDRESS", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON_CARS", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON_CARS", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PERSON_CARS", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_GROUP", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_GROUP", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_GROUP", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_PERMISSION_GROUP", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_PERMISSION_GROUP", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_PERMISSION_GROUP", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROVINCE", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROVINCE", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_PROVINCE", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_SUBDISTRICT", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_SUBDISTRICT", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_SUBDISTRICT", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_TITLE", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_TITLE", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_TITLE", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_USER", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_USER", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_USER", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_CALL_D", "FINISH_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_CALL_D", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_CALL_D", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_CALL_D", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_CALL_H", "RECEIVE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_CALL_H", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_CALL_H", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_CALL_H", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_USER_PROGRAM_PERMISSION", "CREATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_USER_PROGRAM_PERMISSION", "UPDATE_BY", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.TR_USER_PROGRAM_PERMISSION", "DELETE_BY", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TR_USER_PROGRAM_PERMISSION", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_USER_PROGRAM_PERMISSION", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_USER_PROGRAM_PERMISSION", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_CALL_H", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_CALL_H", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_CALL_H", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_CALL_H", "RECEIVE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_CALL_D", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_CALL_D", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_CALL_D", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.TR_CALL_D", "FINISH_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_USER", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_USER", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_USER", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_TITLE", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_TITLE", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_TITLE", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_SUBDISTRICT", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_SUBDISTRICT", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_SUBDISTRICT", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROVINCE", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROVINCE", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROVINCE", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_PERMISSION_GROUP", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_PERMISSION_GROUP", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_PERMISSION_GROUP", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_GROUP", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_GROUP", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM_GROUP", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PROGRAM", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON_CARS", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON_CARS", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON_CARS", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON_ADDRESS", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON_ADDRESS", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON_ADDRESS", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_PERSON", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_DISTRICT", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_DISTRICT", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_DISTRICT", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_DEPARTMENT", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_DEPARTMENT", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_DEPARTMENT", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CAR_BRAND", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CAR_BRAND", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CAR_BRAND", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION_TYPE", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION_TYPE", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION_TYPE", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_CHANNEL", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_CHANNEL", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_CHANNEL", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_ANSWER", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_ANSWER", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_ANSWER", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
        }
    }
}
