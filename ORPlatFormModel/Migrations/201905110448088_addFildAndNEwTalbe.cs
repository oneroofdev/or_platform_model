namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFildAndNEwTalbe : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_CAR_SERIES",
                c => new
                    {
                        SERIES_ID = c.Int(nullable: false, identity: true),
                        BRAND_ID = c.Int(nullable: false),
                        SERIES_NAME = c.String(maxLength: 50, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.SERIES_ID);
            
            AddColumn("dbo.MT_PERSON", "COMPANY_NAME", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.MT_PERSON", "PHONE", c => c.String(maxLength: 30, unicode: false));
            AddColumn("dbo.MT_PERSON", "CUST_TYPE", c => c.String(maxLength: 1, unicode: false));
            AddColumn("dbo.MT_PERSON_CARS", "YEAR", c => c.String(maxLength: 4, unicode: false));
            AddColumn("dbo.MT_PERSON_CARS", "PROVINCE_CODE", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_PERSON_CARS", "PROVINCE_CODE");
            DropColumn("dbo.MT_PERSON_CARS", "YEAR");
            DropColumn("dbo.MT_PERSON", "CUST_TYPE");
            DropColumn("dbo.MT_PERSON", "PHONE");
            DropColumn("dbo.MT_PERSON", "COMPANY_NAME");
            DropTable("dbo.MT_CAR_SERIES");
        }
    }
}
