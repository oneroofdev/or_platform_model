namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetbperson_cars : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MT_PERSON_CARS", "REGISTER_NO", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MT_PERSON_CARS", "REGISTER_NO", c => c.String(maxLength: 10, unicode: false));
        }
    }
}
