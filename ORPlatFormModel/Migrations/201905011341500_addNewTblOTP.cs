namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNewTblOTP : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_EMPLOYEE",
                c => new
                    {
                        EMP_ID = c.Int(nullable: false, identity: true),
                        CORP_ID = c.Int(nullable: false),
                        CORP_EMP_ID = c.Int(),
                        TITLE_ID = c.Int(),
                        FNAME_TH = c.String(maxLength: 100, unicode: false),
                        LNAME_TH = c.String(maxLength: 100, unicode: false),
                        FNAME_EN = c.String(maxLength: 100, unicode: false),
                        LNAME_EN = c.String(maxLength: 100, unicode: false),
                        DEPT_ID = c.Int(nullable: false),
                        POSITION_ID = c.Int(),
                        IS_CONTACT = c.String(maxLength: 1, unicode: false),
                        TEL = c.String(maxLength: 50, unicode: false),
                        MOBILE = c.String(maxLength: 10, unicode: false),
                        FAX = c.String(maxLength: 50, unicode: false),
                        EMAIL = c.String(maxLength: 100, unicode: false),
                        REFER_EMP_CODE = c.String(maxLength: 20, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.EMP_ID);
            
            CreateTable(
                "dbo.MT_EMPLOYEE_HEAD",
                c => new
                    {
                        TRAN_ID = c.Int(nullable: false, identity: true),
                        EMP_ID = c.Int(nullable: false),
                        EMP_ID_HEAD = c.Int(nullable: false),
                        START_DATE = c.DateTime(),
                        END_DATE = c.DateTime(),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TRAN_ID);
            
            CreateTable(
                "dbo.MT_SEND_CHANNEL",
                c => new
                    {
                        SEND_CHANNEL_ID = c.Long(nullable: false, identity: true),
                        SEND_CHANNEL_NAME = c.String(maxLength: 30),
                        ACTIVE = c.String(maxLength: 1),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.SEND_CHANNEL_ID);
            
            CreateTable(
                "dbo.MT_SEND_CONFIG",
                c => new
                    {
                        SEND_ID = c.Long(nullable: false, identity: true),
                        DEFINE_TIME_OUT = c.Int(nullable: false),
                        OTP_REF_CODE = c.String(maxLength: 30),
                        SEND_CHANNEL_ID = c.Long(nullable: false),
                        TEMPLATE_ID = c.Long(nullable: false),
                        ACTIVE = c.String(maxLength: 1),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.SEND_ID);
            
            CreateTable(
                "dbo.MT_SEND_TEMPLATE",
                c => new
                    {
                        TEMPLATE_ID = c.Long(nullable: false, identity: true),
                        TEMPLATE_TEXT = c.String(maxLength: 200),
                        ACTIVE = c.String(maxLength: 1),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TEMPLATE_ID);
            
            CreateTable(
                "dbo.TR_SEND_OTP",
                c => new
                    {
                        LOG_ID = c.Long(nullable: false, identity: true),
                        USER_LOGIN_NAME = c.String(maxLength: 100),
                        REF_CODE = c.String(maxLength: 4),
                        PASS_OTP = c.String(maxLength: 6),
                        PASS_INPUT = c.String(maxLength: 6),
                        SEND_TO = c.String(maxLength: 10),
                        STATUS_OTP = c.String(maxLength: 1),
                        OTP_SEND_TIME = c.DateTime(),
                        OTP_TIME_OUT = c.DateTime(),
                        TEMPLATE_ID = c.Long(nullable: false),
                        SEND_CHANNEL_ID = c.Long(nullable: false),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.LOG_ID);
            
            //AddColumn("dbo.MT_PERSON", "MOBILE_SMS", c => c.String(maxLength: 10, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_PERSON", "MOBILE_SMS");
            DropTable("dbo.TR_SEND_OTP");
            DropTable("dbo.MT_SEND_TEMPLATE");
            DropTable("dbo.MT_SEND_CONFIG");
            DropTable("dbo.MT_SEND_CHANNEL");
            DropTable("dbo.MT_EMPLOYEE_HEAD");
            DropTable("dbo.MT_EMPLOYEE");
        }
    }
}
