namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updCorpID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_CALL_ANSWER", "CORP_ID", c => c.Int());
            AddColumn("dbo.MT_CALL_QUESTION", "CORP_ID", c => c.Int());
            AddColumn("dbo.MT_CALL_QUESTION_TYPE", "CORP_ID", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_CALL_QUESTION_TYPE", "CORP_ID");
            DropColumn("dbo.MT_CALL_QUESTION", "CORP_ID");
            DropColumn("dbo.MT_CALL_ANSWER", "CORP_ID");
        }
    }
}
