namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updCorpIdTrCallH : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_CALL_H", "CORP_ID", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_CALL_H", "CORP_ID");
        }
    }
}
