namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewTableMaster : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_CAR_BRAND",
                c => new
                    {
                        BRAND_ID = c.Int(nullable: false, identity: true),
                        BRAND_NAME = c.String(maxLength: 50, unicode: false),
                        CREATE_BY = c.String(maxLength: 30, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 30, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 30, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.BRAND_ID);
            
            CreateTable(
                "dbo.MT_DISTRICT",
                c => new
                    {
                        DISTRICT_CODE = c.Int(nullable: false),
                        TRAN_ID = c.Int(nullable: false, identity: true),
                        PROVINCE_CODE = c.Int(nullable: false),
                        DISTRICT_NAME_TH = c.String(maxLength: 200, unicode: false),
                        DISTRICT_NAME_EN = c.String(maxLength: 200, unicode: false),
                        CREATE_BY = c.String(maxLength: 30, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 30, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 30, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.DISTRICT_CODE);
            
            CreateTable(
                "dbo.MT_PERSON_CARS",
                c => new
                    {
                        TRAN_ID = c.Int(nullable: false, identity: true),
                        PERSON_ID = c.Int(nullable: false),
                        REGISTER_NO = c.String(maxLength: 10, unicode: false),
                        BRAND_NAME = c.String(maxLength: 50, unicode: false),
                        SERIE = c.String(maxLength: 50, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.String(maxLength: 30, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 30, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 30, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TRAN_ID);
            
            CreateTable(
                "dbo.MT_PROVINCE",
                c => new
                    {
                        PROVINCE_CODE = c.Int(nullable: false),
                        TRAN_ID = c.Int(nullable: false, identity: true),
                        PROV_NAME_TH = c.String(maxLength: 200, unicode: false),
                        PROV_NAME_EN = c.String(maxLength: 200, unicode: false),
                        CREATE_BY = c.String(maxLength: 30, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 30, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 30, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.PROVINCE_CODE);
            
            CreateTable(
                "dbo.MT_SUBDISTRICT",
                c => new
                    {
                        SUBDISTRICT_CODE = c.Int(nullable: false),
                        TRAN_ID = c.Int(nullable: false, identity: true),
                        DISTRICT_CODE = c.Int(nullable: false),
                        SUBDISTRICT_NAME_TH = c.String(maxLength: 200, unicode: false),
                        SUBDISTRICT_NAME_EN = c.String(maxLength: 200, unicode: false),
                        ZIPCODE = c.String(maxLength: 200, unicode: false),
                        CREATE_BY = c.String(maxLength: 30, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 30, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 30, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.SUBDISTRICT_CODE);
            
            CreateTable(
                "dbo.MT_TITLE",
                c => new
                    {
                        TITLE_ID = c.Int(nullable: false, identity: true),
                        TITLE_NAME_TH = c.String(maxLength: 50, unicode: false),
                        TITLE_NAME_EN = c.String(maxLength: 50, unicode: false),
                        CREATE_BY = c.String(maxLength: 30, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 30, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 30, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TITLE_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MT_TITLE");
            DropTable("dbo.MT_SUBDISTRICT");
            DropTable("dbo.MT_PROVINCE");
            DropTable("dbo.MT_PERSON_CARS");
            DropTable("dbo.MT_DISTRICT");
            DropTable("dbo.MT_CAR_BRAND");
        }
    }
}
