namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updaebranch : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_BRANCH",
                c => new
                    {
                        BRANCH_ID = c.Int(nullable: false, identity: true),
                        BRANCH_NAME_TH = c.String(maxLength: 50, unicode: false),
                        BRANCH_NAME_EN = c.String(maxLength: 50, unicode: false),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.BRANCH_ID);
            
            AddColumn("dbo.MT_CALL_QUESTION", "IS_BRANCH", c => c.String(maxLength: 1, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_CALL_QUESTION", "IS_BRANCH");
            DropTable("dbo.MT_BRANCH");
        }
    }
}
