namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldPerSon_v2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_PERSON", "PRIORITY", c => c.String(maxLength: 1, unicode: false));
            AddColumn("dbo.MT_PERSON", "MEMBER_TIER", c => c.String(maxLength: 3, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_PERSON", "MEMBER_TIER");
            DropColumn("dbo.MT_PERSON", "PRIORITY");
        }
    }
}
