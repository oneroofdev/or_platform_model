namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCorpProfile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_CORPORATE",
                c => new
                    {
                        CORP_ID = c.Int(nullable: false, identity: true),
                        CORP_NAME_TH = c.String(maxLength: 200, unicode: false),
                        CORP_NAME_EN = c.String(maxLength: 200, unicode: false),
                        CORP_SHORT_NAME = c.String(maxLength: 10, unicode: false),
                        CORP_TEL = c.String(maxLength: 50, unicode: false),
                        CORP_FAX = c.String(maxLength: 50, unicode: false),
                        CORP_TAX_ID = c.String(maxLength: 13, unicode: false),
                        LOGO_PATH = c.String(unicode: false, storeType: "text"),
                        CONTRACT_NO = c.String(maxLength: 50, unicode: false),
                        CORP_TOTAL_ACCOUNT = c.Int(),
                        TOTAL_PROJECT_AMOUNT = c.Int(),
                        CONTRACT_START_DATE = c.DateTime(),
                        CONTRACT_END_DATE = c.DateTime(),
                        DELIVER_LOGIN_DATE = c.DateTime(),
                        REMARK = c.String(maxLength: 200, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.CORP_ID);
            
            CreateTable(
                "dbo.MT_CORPORATE_ADDRESS",
                c => new
                    {
                        CORP_ADDR_ID = c.Int(nullable: false, identity: true),
                        CORP_ID = c.Int(nullable: false),
                        CORP_ADDR_TYPE = c.String(maxLength: 2, unicode: false),
                        ADDR_NO = c.String(maxLength: 50, unicode: false),
                        BUILDING_NAME = c.String(maxLength: 100, unicode: false),
                        FLOOR = c.String(maxLength: 50, unicode: false),
                        VILLAGE = c.String(maxLength: 100, unicode: false),
                        MOO = c.String(maxLength: 50, unicode: false),
                        SOI = c.String(maxLength: 50, unicode: false),
                        ROAD = c.String(maxLength: 50, unicode: false),
                        SUBDISTRICT_CODE = c.Int(),
                        DISTRICT_CODE = c.Int(),
                        PROVINCE_CODE = c.Int(),
                        ZIPCODE = c.String(maxLength: 5, unicode: false),
                        TEL = c.String(maxLength: 10, unicode: false),
                        FAX = c.String(maxLength: 10, unicode: false),
                        CREATE_BY = c.String(maxLength: 50, unicode: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.String(maxLength: 50, unicode: false),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 50, unicode: false),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.CORP_ADDR_ID, t.CORP_ID });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MT_CORPORATE_ADDRESS");
            DropTable("dbo.MT_CORPORATE");
        }
    }
}
