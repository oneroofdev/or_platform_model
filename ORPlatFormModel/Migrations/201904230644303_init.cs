namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LOG_API",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Request = c.String(),
                        Response = c.String(),
                        Status = c.String(maxLength: 50),
                        RequestDateTime = c.DateTime(),
                        ResponseDateTime = c.DateTime(),
                        ModuleType = c.String(),
                        ActionType = c.String(),
                        UserAccount = c.String(maxLength: 100),
                        IP = c.String(maxLength: 50),
                        CreatedBy = c.String(maxLength: 30),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 30),
                        ModifiedDate = c.DateTime(),
                        DeleteBy = c.String(maxLength: 30),
                        DeleteDate = c.DateTime(),
                        TransactionId = c.String(maxLength: 50),
                        test_api = c.String(maxLength: 50),
                        test_api2 = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MT_DEPARTMENT",
                c => new
                    {
                        DEPT_ID = c.Int(nullable: false, identity: true),
                        CORP_ID = c.Int(nullable: false),
                        CORP_DEPT_ID = c.Int(),
                        DEPT_NAME_TH = c.String(maxLength: 100, unicode: false),
                        DEPT_NAME_EN = c.String(maxLength: 100, unicode: false),
                        IS_EXECUTIVE = c.String(maxLength: 1, unicode: false),
                        VIEW_ALL_PROJECT = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.DEPT_ID);
            
            CreateTable(
                "dbo.MT_PROGRAM",
                c => new
                    {
                        PROGRAM_ID = c.Int(nullable: false, identity: true),
                        DISPLAY_SEQ = c.Int(nullable: false),
                        NAME = c.String(maxLength: 100, unicode: false),
                        DESCRIPTION = c.String(maxLength: 255, unicode: false),
                        URL = c.String(maxLength: 255, unicode: false),
                        GRP_MENU_ID = c.Int(nullable: false),
                        ICON_MENU = c.String(maxLength: 100, unicode: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.PROGRAM_ID);
            
            CreateTable(
                "dbo.MT_PROGRAM_GROUP",
                c => new
                    {
                        GRP_MENU_ID = c.Int(nullable: false, identity: true),
                        GRP_MENU_NAME = c.String(maxLength: 100, unicode: false),
                        DISPLAY_ON_PAGE = c.String(maxLength: 2, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.GRP_MENU_ID);
            
            CreateTable(
                "dbo.MT_PROGRAM_PERMISSION_GROUP",
                c => new
                    {
                        TRAN_ID = c.Int(nullable: false, identity: true),
                        CORP_ID = c.Int(nullable: false),
                        PROGRAM_ID = c.Int(nullable: false),
                        DEPT_ID = c.Int(nullable: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TRAN_ID);
            
            CreateTable(
                "dbo.MT_USER",
                c => new
                    {
                        USER_ID = c.Int(nullable: false, identity: true),
                        EMP_ID = c.Int(nullable: false),
                        USER_LOGIN_NAME = c.String(maxLength: 100, unicode: false),
                        PASSWORD = c.String(maxLength: 60, unicode: false),
                        DEPT_ID = c.Int(nullable: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.USER_ID);
            
            CreateTable(
                "dbo.TR_USER_PROGRAM_PERMISSION",
                c => new
                    {
                        TRAN_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        PROGRAM_ID = c.Int(nullable: false),
                        DEPT_ID = c.Int(nullable: false),
                        ACTIVE = c.String(maxLength: 1, unicode: false),
                        CREATE_BY = c.Int(nullable: false),
                        CREATE_DATE = c.DateTime(nullable: false),
                        UPDATE_BY = c.Int(),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.Int(),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TRAN_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_USER_PROGRAM_PERMISSION");
            DropTable("dbo.MT_USER");
            DropTable("dbo.MT_PROGRAM_PERMISSION_GROUP");
            DropTable("dbo.MT_PROGRAM_GROUP");
            DropTable("dbo.MT_PROGRAM");
            DropTable("dbo.MT_DEPARTMENT");
            DropTable("dbo.LOG_API");
        }
    }
}
