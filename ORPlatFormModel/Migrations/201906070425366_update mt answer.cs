namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatemtanswer : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MT_CALL_ANSWER", "ANSWER_DESC", c => c.String(maxLength: 500, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MT_CALL_ANSWER", "ANSWER_DESC", c => c.String(maxLength: 150, unicode: false));
        }
    }
}
