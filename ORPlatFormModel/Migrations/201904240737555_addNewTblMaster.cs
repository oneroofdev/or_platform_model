namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNewTblMaster : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MT_CALL_QUESTION", "QUESTION_DESC", c => c.String(maxLength: 150, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "CREATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "UPDATE_BY", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "DELETE_BY", c => c.String(maxLength: 30, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MT_CALL_QUESTION", "DELETE_BY", c => c.Int());
            AlterColumn("dbo.MT_CALL_QUESTION", "UPDATE_BY", c => c.Int());
            AlterColumn("dbo.MT_CALL_QUESTION", "CREATE_BY", c => c.Int(nullable: false));
            AlterColumn("dbo.MT_CALL_QUESTION", "QUESTION_DESC", c => c.String(maxLength: 100, unicode: false));
        }
    }
}
