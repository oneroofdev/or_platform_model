namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfieldis_maintbprogram : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_PROGRAM", "IS_MAIN", c => c.String(maxLength: 1, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_PROGRAM", "IS_MAIN");
        }
    }
}
