namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updaebranchinquestionActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_BRANCH", "ACTIVE", c => c.String(maxLength: 1, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_BRANCH", "ACTIVE");
        }
    }
}
