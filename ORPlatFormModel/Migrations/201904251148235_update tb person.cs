namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetbperson : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MT_PERSON", "PRIORITY", c => c.String(maxLength: 3, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MT_PERSON", "PRIORITY", c => c.String(maxLength: 1, unicode: false));
        }
    }
}
