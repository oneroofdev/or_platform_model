namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updaebranchinquestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_BRANCH", "AREA", c => c.String(maxLength: 200, unicode: false));
            AddColumn("dbo.TR_CALL_D", "BRANCH_ID", c => c.Int());
            AlterColumn("dbo.MT_BRANCH", "BRANCH_NAME_TH", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.MT_BRANCH", "BRANCH_NAME_EN", c => c.String(maxLength: 100, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MT_BRANCH", "BRANCH_NAME_EN", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.MT_BRANCH", "BRANCH_NAME_TH", c => c.String(maxLength: 50, unicode: false));
            DropColumn("dbo.TR_CALL_D", "BRANCH_ID");
            DropColumn("dbo.MT_BRANCH", "AREA");
        }
    }
}
