namespace ORPlatFormModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFiledPerSon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_PERSON", "PERSON_CODE", c => c.String(maxLength: 10, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_PERSON", "PERSON_CODE");
        }
    }
}
