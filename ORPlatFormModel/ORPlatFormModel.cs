namespace DB_MODEL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using global::ORPlatFormModel.Models;

    public partial class ORPlatFormModel : DbContext
    {
        public ORPlatFormModel()
            : base("name=ORPlatFormModel")
        {
        }
        /// <summary>
        /// Crop Profile
        /// </summary>
        public virtual DbSet<MT_CORPORATE_ADDRESS> MT_CORPORATE_ADDRESS { get; set; } //
        public virtual DbSet<MT_CORPORATE> MT_CORPORATE { get; set; } //

        public virtual DbSet<LOG_API> LOG_API { get; set; }

        /// <summary>
        /// User Login
        /// </summary>
        public virtual DbSet<MT_USER> MT_USER { get; set; } //
        public virtual DbSet<MT_DEPARTMENT> MT_DEPARTMENT { get; set; } //
        public virtual DbSet<MT_PROGRAM_PERMISSION_GROUP> MT_PROGRAM_PERMISSION_GROUP { get; set; } //
        public virtual DbSet<TR_USER_PROGRAM_PERMISSION> TR_USER_PROGRAM_PERMISSION { get; set; } //
        public virtual DbSet<MT_PROGRAM> MT_PROGRAM { get; set; } //
        public virtual DbSet<MT_PROGRAM_GROUP> MT_PROGRAM_GROUP { get; set; } //

        /// <summary>
        /// person
        /// </summary>
        public virtual DbSet<MT_PERSON> MT_PERSON { get; set; } //
        public virtual DbSet<MT_PERSON_ADDRESS> MT_PERSON_ADDRESS { get; set; } //
        public virtual DbSet<MT_TITLE> MT_TITLE { get; set; } // 
        public virtual DbSet<MT_CAR_BRAND> MT_CAR_BRAND { get; set; } // 
        public virtual DbSet<MT_PERSON_CARS> MT_PERSON_CARS { get; set; } // 
        public virtual DbSet<MT_CAR_SERIES> MT_CAR_SERIES { get; set; } //  

        /// <summary>
        /// CallCenter
        /// </summary> 
        public virtual DbSet<MT_CALL_ANSWER> MT_CALL_ANSWER { get; set; } //
        public virtual DbSet<MT_CALL_CHANNEL> MT_CALL_CHANNEL { get; set; } //
        public virtual DbSet<MT_CALL_QUESTION_TYPE> MT_CALL_QUESTION_TYPE { get; set; } //
        public virtual DbSet<MT_CALL_QUESTION> MT_CALL_QUESTION { get; set; } //
        public virtual DbSet<TR_CALL_D> TR_CALL_D { get; set; } // 
        public virtual DbSet<TR_CALL_H> TR_CALL_H { get; set; } //

        /// <summary>
        /// Master
        /// </summary> 
        public virtual DbSet<MT_PROVINCE> MT_PROVINCE { get; set; } //
        public virtual DbSet<MT_DISTRICT> MT_DISTRICT { get; set; } // 
        public virtual DbSet<MT_SUBDISTRICT> MT_SUBDISTRICT { get; set; } //

        //DB SEND OTP
        public virtual DbSet<MT_SEND_CHANNEL> MT_SEND_CHANNEL { get; set; }
        public virtual DbSet<MT_SEND_CONFIG> MT_SEND_CONFIG { get; set; }
        public virtual DbSet<MT_SEND_TEMPLATE> MT_SEND_TEMPLATE { get; set; }
        public virtual DbSet<TR_SEND_OTP> TR_SEND_OTP { get; set; }
        public virtual DbSet<MT_EMPLOYEE_HEAD> MT_EMPLOYEE_HEAD { get; set; }
        public virtual DbSet<MT_EMPLOYEE> MT_EMPLOYEE { get; set; }
        public virtual DbSet<MT_BRANCH> MT_BRANCH { get; set; } 

        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new DecimalPrecisionAttributeConvention());
        }
    }
}
